import React from "react"
import {history} from "../../../history";
import { setLoginFlag} from "../../../redux/actions/auth/loginAction";
import { store} from "../../../redux/storeConfig/store"
import './login.css';
class Login extends React.Component{
  render(){
    return <h4 className="sampleLoginText" onClick={() =>{ localStorage.setItem("reactpanel", JSON.stringify({name : "Pruthvi"})) ; store.dispatch(setLoginFlag(true)); history.push("/home")}}>You're Login Here. Click here</h4>
  }
}

export default Login